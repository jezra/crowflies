
function toRadians(degree) {
  var pi = Math.PI;
  return degree * (pi/180);
}

function toDegrees(r) {
  var pi = Math.PI
  return r * (180/pi);
}

function computeBearingDistance(p1, p2) {
  let R = 6371e3;
  // see: http://www.movable-type.co.uk/scripts/latlong.html

  /* compute the  distance with haversine */

  //get the lat lng in radians
  var p1LatRad = toRadians(p1.latitude);
  var p2LatRad = toRadians(p2.latitude);
  var p1LngRad = toRadians(p1.longitude);
  var p2LngRad = toRadians(p2.longitude);
  var latDiffRad = toRadians(p2.latitude-p1.latitude);
  var lonDiffRad = toRadians(p2.longitude-p1.longitude);

  // use haversine formula
  var a = Math.sin(latDiffRad/2) * Math.sin(latDiffRad/2) +
      Math.cos(p1LatRad) * Math.cos(p2LatRad) *
      Math.sin(lonDiffRad/2) * Math.sin(lonDiffRad/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var distance = R * c;

  //compute bearing
  var y = Math.sin(p2LngRad-p1LngRad) * Math.cos(p2LatRad);
  var x = Math.cos(p1LatRad)*Math.sin(p2LatRad) -
          Math.sin(p1LatRad)*Math.cos(p2LatRad)*Math.cos(p2LngRad-p1LngRad);
  var angle = Math.atan2(y, x);
  var bearing = (angle*180/Math.PI + 360) % 360;
  return {"distance": distance, "bearing": bearing}
}

