/*
copyright 2020 Jezra
source released under GPLv3
*/

//globals
userLocations = [];
deviceLatitude = undefined;
deviceLongitude = undefined;
directionBearing = 0;

//handle device orientation events
window.addEventListener("deviceorientationabsolute", handleOrientation, true);

// make it easier to find element by id
function _id(s) {
  return document.getElementById(s);
}

// disable/enable based on id
function disable(id) {
    _id(id).disabled = true;
}
function enable(id) {
    _id(id).disabled = false;
}
//show / hide by id
function show(id) {
  _id(id).style.visibility = "visible";
}

function hide(id) {
  _id(id).style.visibility = "hidden";
}


function clickedLocationAdd(){
  //show the modal
  show('locationModal');
  // create a sane default name
  _id('locationName').value = `Location #${userLocations.length+1}`;
  //clear the lat lng
  _id('locationLat').value = 0;
  _id('locationLng').value = 0;
}

function clickedLocationDelete(){
  // show the delete confirmation
  show('deleteConfirmationModal');
  //set the 'selectedLocationName' value
  _id('selectedLocationName').innerHTML = userLocations[selectedLocationIndex()].name;
}

function clickedCancelLocationAdd(){
  //hide the location modal
  hide('locationModal');
  // set the select to -- locations --
  _id('locations').selectedIndex = 0;
}

function changedLocationSelect(val){
  //add location?
  if ( val == "") {
    //store the index as undefined
    selectedLocationIndex(undefined);
    //hide unneeded UI
    hide("locationDelete");
    hide("locationPointer");

  } else if (val == "add"){
    clickedLocationAdd();
  } else {
    // store the index as an integer
    selectedLocationIndex( parseInt(val) );
    // show relevant UI elements
    show("locationDelete");
    show("locationPointer");
  }
}

function selectedLocationIndex(val){
  // is there a val?
  if (val!=undefined) {
    //write the val to localstorage
    localStorage.setItem("selectedLocationIndex", val);
  } else {
    //return the value of the selectedLocation from localStorage
    return localStorage.getItem("selectedLocationIndex");
  }
}

function loadUserLocations() {
  let item = localStorage.getItem("userLocations");
  if (item == undefined) {
    userLocations = [];
  } else {
    // parse the item
    userLocations = JSON.parse(item);
  }
}

function saveUserLocations() {
  let item = JSON.stringify(userLocations);
  //save item to localStorage
  localStorage.setItem("userLocations", item);
}

function postLoad(){
  // start getting gps information
  getGPS();
}

function init(){
  //disable the 'locationDelete' button
  hide("locationDelete");
  //hide the crow
  hide("locationPointer");
  disable("useDeviceLocation");
  //hide the stats
  hide('hideStatsLink')
  //load the locations
  loadUserLocations();
  updateUserLocationSelect();

  //this should do nothing on Android?
  postLoad();

  // update the UI based on the stored selectedLocationIndex
  let index = selectedLocationIndex();
  if (index >= 0){
    setLocationSelect(index);
  }
}

function setLocationSelect(index) {
 // set the select
  _id('locations').value = index;
  // emulate a change
  changedLocationSelect(index);
}

function updateUserLocationSelect(){
  let locations =  _id("locations");
  //clear the userLocations optGroup
  locations.innerHTML = "";
  // add the -- Locations --
  var opt = document.createElement("option");
  opt.value = "";
  opt.text = "-- Locations --";
  locations.appendChild(opt);

  // fill the userLocations optGroup with user locations
  for (var i = 0; i<userLocations.length; i++) {
    var name = userLocations[i].name;
    var opt = document.createElement("option");
    opt.value = i;
    opt.text = name;
    //append the opt to the userLocations
   locations.appendChild(opt);
  }
  // add the Add location
  var opt = document.createElement("option");
    opt.value = "add";
    opt.text = "Add location ...";
    locations.appendChild(opt);
}

// ensure new location form has decent data before enabling the save button
function validateNewLocation() {
  let nameVal = _id('locationName').value;
  let latVal = _id('locationLat').value;
  let lngVal = _id('locationLng').value;

  if (nameVal && latVal && lngVal) {
    // there is data
    _id("newLocationSave").disabled = false;
  } else {
   _id("newLocationSave").disabled = true;
  }
}

function clickedUseDeviceLocation(){
  // add the deviceLatitude/Lng to the form
  _id('locationLat').value = deviceLatitude;
  _id('locationLng').value = deviceLongitude;
  //validate
  validateNewLocation() ;
}

function getGPS(){
  var options = { enableHighAccuracy: true, timeout: 1000};
  navigator.geolocation.getCurrentPosition(positionDetected, positionError, options);
  // get the GPS again in 5 seconds
  setTimeout(function(){ getGPS(); }, 1000);
}

function positionDetected(position) {
  //enable the "use device location" button
  enable('useDeviceLocation');
  var coords = position.coords;
  deviceLatitude = coords.latitude;
  deviceLongitude = coords.longitude;
  _id("lat").innerHTML = "Lat: "+deviceLatitude;
  _id("lng").innerHTML = "Lng: "+deviceLongitude;
  //enable the 'use device location button'
  enable("useDeviceLocation");
  _id("gpsAccuracy").innerHTML = `Acc: ${coords.accuracy} meters`;

  let selectedIndex = selectedLocationIndex();
  _id('data').innerHTML = "index: "+selectedIndex;
  // is there a user selected location?
  if (selectedIndex >= 0) {
    var distanceUnit = "M";
    //get the selected location
    let target = userLocations[selectedIndex];
    //get the bearing and distance
    let bc = computeBearingDistance(coords, target);
    let d = bc.distance;
    _id("bearing").innerHTML = "Bearing: "+bc.bearing;
    _id("distance").innerHTML = "Distance: "+d;
    //format the distance..
    if (d>1000){
      distanceUnit = "K";
      d = d/1000
    }
    //round to 1 decimal place
    d = Math.round(d*10)/10;
    //set the text
    _id("locationDistance").innerHTML = `${d} ${distanceUnit}`;
    //rotate the crow!
     _id("locationPointer").style.transform = `rotate(${bc.bearing}deg)`;
     // keep track of the bearing
     directionBearing = bc.bearing;

  }
}

function positionError(err) {
  //disable the "use device location" button, there is no data
  disable('useDeviceLocation');
}

function clickedShowStats() {
  show('hideStatsLink');
  hide('showStatsLink');
}
function clickedHideStats() {
  show('showStatsLink');
  hide('hideStatsLink');
}

function clickedNewLocationSave(){
  // get the values
  let nameVal = _id('locationName').value;
  let latVal = _id('locationLat').value;
  let lngVal = _id('locationLng').value;
  // what is the accuracy?
  let acc = 0;
  let gpsAcc = parseInt( _id("gpsAccuracy").innerHTML);
  if (gpsAcc > 0 ) {
    acc = gpsAcc;
  }
  //create the location
  let loc = {"name":nameVal, "latitude":latVal,"longitude":lngVal, "accuracy":  acc }
  //add the location to the userLocations
  userLocations.push(loc);
  //save
  saveUserLocations();
  //update
  updateUserLocationSelect();
  //hide the modal
  hide('locationModal');

  // if there is a currently selected location, reselect
  let index = selectedLocationIndex();
  if (index >= 0 ){
    setLocationSelect(index);
  }
}

function clickedCancelLocationDelete() {
  hide('deleteConfirmationModal');
}

function clickedConfirmLocationDelete(){
  //close the modal
  hide('deleteConfirmationModal');
  //get the selected index
  let selectedIndex = selectedLocationIndex();
  //remove the selectedLocationIndex from userlocations
  userLocations.splice(selectedIndex,1);
  // save the new user locations
  saveUserLocations();
  //reset selectedLocationIndex
  selectedLocationIndex(undefined);
  //update the user locations Select
  updateUserLocationSelect();
  // hide UI elements that are no longer providing useful functions
  hide("locationDelete");
  hide("locationPointer");
}


function handleOrientation(event) {
  var absolute = event.absolute;
  var alpha    = event.alpha;
  var beta     = event.beta;
  var gamma    = event.gamma;

  // Do stuff with the new orientation data
  /*
  _id("alpha").innerHTML = alpha;
  _id("beta").innerHTML = beta;
   _id("gamma").innerHTML = gamma;
   */
  // rotate the directionDisplay (including crow)
  _id("directionDisplay").style.transform = `rotate(${alpha}deg)`;
  // unrotate the distance
  let unrotate = -alpha - directionBearing;
  _id("locationDistance").style.transform = `rotate(${unrotate}deg)`;
}

