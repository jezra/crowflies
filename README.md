# CrowFlies
CrowFlies is a mobile web app that helps the user to find the distance and bearing to a given location
based on GPS coordinates. The location can be entered manually, or the GPS
coordinates can be fetched from the mobile device. 

see:
<https://crowflies.jezra.net>

## Limitations
CrowFlies will not work in Firefox, due to Firefox's lack of support for retrieving
sensor data from a mobile phone's compass sensor.
